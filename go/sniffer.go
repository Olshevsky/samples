package main

import (
    "fmt"
    "github.com/google/gopacket"
    "github.com/google/gopacket/pcap"
    "log"
    "os"
    "bytes"
    "net/http"
    "encoding/json"
    "strings"
    "time"
    "regexp"
)

type Configuration struct {
  Endpoint                    string
  ClientId                    string
  ApplicationPermissions      string
}

type Request struct {
    Client                    string          `json:"client"`
    Basic                     string          `json:"basic"`
    Payload                  []string         `json:"payload"`
}

var (
    snapshot_len int32  = 1024
    promiscuous  bool   = false
    err          error
    timeout      time.Duration = 30 * time.Second
    handle       *pcap.Handle
    configuration Configuration = Configuration{}
)

func handler(channel chan *Request) {
  for {
        _r := <- channel
        fmt.Println("Data received", configuration.Endpoint);
        data, _ := json.Marshal(_r);
        resp, _ := http.Post(configuration.Endpoint, "application/json", bytes.NewReader(data))
        defer func () {
          if resp.Body != nil {
            resp.Body.Close()
          }
        }()
    }
}

func main() {
  file, _ := os.Open("config.json")
  decoder := json.NewDecoder(file)
  decoder.Decode(&configuration)
  requestChannel := make(chan *Request)

  fmt.Println("Sniffer v1.0 - Parental Controll | ", configuration.Endpoint)
  go handler(requestChannel);

  devices, err := pcap.FindAllDevs()
  if err != nil {
        log.Fatal(err)
    }
    var d = devices[0];
    var device = d.Name;

    handle, err = pcap.OpenLive(device, snapshot_len, promiscuous, timeout)
    if err != nil {log.Fatal(err) }
    defer handle.Close()

    packetSource := gopacket.NewPacketSource(handle, handle.LinkType())
    for packet := range packetSource.Packets() {
        applicationLayer := packet.ApplicationLayer()
        if applicationLayer != nil {
            if strings.Contains(string(applicationLayer.Payload()), "HTTP") {
                var payload = string(applicationLayer.Payload());
                payload = strings.Replace(payload, "\r", "", -1);
                var request = strings.Split(payload, "\n");
                match, _ := regexp.MatchString("GET", request[0])
                if(match) {
                  _r := &Request{
                    Client: configuration.ClientId,
                    Basic: request[0]}
                  for part := range request {
                    _match, _ := regexp.MatchString("(Content-Type|User-Agent|Host|Referer|Accept-Language|Accept-Encoding|Origin|Accept|X-Requested-With)", request[part])
                    if(_match) {
                      _r.Payload = append(_r.Payload, request[part])
                    }
                  }
                  requestChannel <- _r
                }
            }
        }
    }
}
