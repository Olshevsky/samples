import React, {Component} from 'react'
// import {listen, observeChange} from 
import {defer} from './../../../base/components/defer'
import IssueStore, {dispatch} from './../../../common/issues/IssueStore'
import {firstLoad, setView} from './../../../common/issues/actions'
import ManagementToolbar from './../components/ManagementToolbar'
import {Grid, List, Board} from './../components/modes/index'
import {Loader, Spinner} from '../../../base/index'
import FloatingCreate from '../../../common/application/components/FloatingCreate'

let modes = {
    grid: Grid,
    list: List,
    board: Board
};

@observeChange(['status'])
@defer(_ => dispatch(firstLoad()))
@listen(IssueStore, ['settings', 'status', 'loaded', 'issues', 'view', 'selected', 'infoTab', 'comments', 'attachments', 'activity', 'notes'])
class CaseManagement extends Component {
    onStatusChange(status) {
        let viewMode = this.state.view;
        dispatch(setView(
            status && status != null ?
                (
                    viewMode == 'board' || viewMode == 'grid' ? 'grid' : 'list'
                ) : (
                    viewMode == 'grid' ? 'board' : 'list'
                )
        ))
    }

    get messages() {
        return Object
            .keys(this.state.comments.byIssue)
            .reduceRight(
                (buffer, element) =>
                    Object.assign({}, buffer, {[element]: (this.state.comments.byIssue[element] || []).map(id => this.state.comments.list[id])}), {}
            );
    }

    get attachments() {
        return Object
            .keys(this.state.attachments.byIssue)
            .reduceRight(
                (buffer, element) =>
                    Object.assign({}, buffer, {[element]: (this.state.attachments.byIssue[element] || []).map(id => this.state.attachments.list[id])}), {}
            );
    }

    get issues() {
        let result = [];
        let completedList =  Object
            .keys(this.state.issues.byStatus)
            .reduceRight(
                (buffer, element) =>
                    Object.assign({}, buffer, {[element]: (this.state.issues.byStatus[element] || []).map(id => this.state.issues.list[id])}), {}
            );
        result =  this.state.status || this.state.status ? completedList[this.state.status] || [] : Object.keys(this.state.issues.list).map(id => this.state.issues.list[id])


        return result.sort((s1, s2) => {
            return s1.index < s2.index ? -1 : 1
        })
    }

    get notes() {
        return Object
            .keys(this.state.notes.byIssue)
            .reduceRight(
                (buffer, element) =>
                    Object.assign({}, buffer, {[element]: (this.state.notes.byIssue[element] || []).map(id => this.state.notes.list[id])}), {}
            );
    }

    get activities() {
        return Object
            .keys(this.state.activity.byIssue)
            .reduceRight(
                (buffer, element) =>
                    Object.assign({}, buffer, {[element]: (this.state.activity.byIssue[element] || []).map(id => this.state.activity.list[id])}), {}
            );
    }

    render() {
        let ViewMode = modes[this.state.view];
        return (
            <div className="case-management">
                <ManagementToolbar />

                <FloatingCreate />
                <Loader className="full-height-container cut-toolbar" spinner={<Spinner gray={true} />} loaded={this.state.loaded}>
                    <ViewMode
                        issues={this.issues}

                        activities={this.activities}
                        attachments={this.attachments}
                        messages={this.messages}
                        notes={this.notes}

                        info={this.state.infoTab}
                        selected={this.state.selected ? this.state.selected.id : undefined}
                    />
                </Loader>
            </div>
        )
    }
}

export default CaseManagement
