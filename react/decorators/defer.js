/** @flow */
import React from 'react'

export function defer(action:Function, _dispatch):Function {
    return Component => {
        class DeferComponent extends Component {
            componentDidMount() {
                super.componentDidMount && super.componentDidMount()
                action(_dispatch, {params: this.props.params, query: this.props.location})
            }
        }

        return DeferComponent
    }
}
