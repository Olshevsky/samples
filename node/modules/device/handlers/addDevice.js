/**
 * Created by adlab on 21.01.16.
 */
export function addDevice(req, res, next) {
    Promise.all([
        req.user,
        req.plan
    ]).then(results => {
        let user = results[0],
            plan = results[1];

        if(plan.devices <= user.stats.devices) {
            res.json({ok: false})
        } else {
            res.json({ok: true})
            User.update({_id: user._id}, {
                stats: {...user.stats, devices: user.stats.devices + 1}
            })
        }
    })
}