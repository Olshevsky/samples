/**
 * Created by adlab on 21.01.16.
 */
import Token from './../documents/Token'
export function injectUser(req, res, next) {
    const tokenId = req.header('Token') || req.query.Token || req.body.Token;
    req.user = Token.find(tokenId).populate('user').exec().then(token => token.owner)
    next()
}