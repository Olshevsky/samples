/**
 * Created by adlab on 20.01.16.
 */
import {Router} from 'express'

import {saveRequest} from './handlers/saveRequest'
import {injectDevice} from './../device/middleware/injectDevice'

export const router = Router();
router.post('/api/1/core/request/:type', injectDevice, saveRequest);
