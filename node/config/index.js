/**
 * Created by adlab on 13.02.16.
 */
import config from './config'
import local from './config.local'

export default {...config, ...local}